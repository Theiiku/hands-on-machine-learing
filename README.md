# -Hands-On-Machine-Learing

机器学习实战 基于Scikit-Learn、Keras和TensorFlow(原书第二版) 例程 增加了一些配图，修改了部分缺失内容的源码，添加了注释

示例中的代码都进行了实机测试

持续更新中，欢迎指正

（诸位写论文的硕博不要过分依赖机器学习，好好实验才是真 :joy:） 

相关信息

操作系统：      Ubuntu 20.04.1

IDE版本：      Jupyter + Visual Studio Code

python 版本：  Python 3.8.10 64-bit

Scikit-Learn：scikit-learn 1.1.3 

Keras：       keras 2.11.0

TensorFlow:   tensorflow 2.11.0

另：项目为了方便使用添加了部分功能，可能和书上有出入